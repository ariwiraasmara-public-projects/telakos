<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\astela_1002_tenant;
use App\Models\astela_1004_room;
use App\Libraries\myfunction;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        $users = [
            ['id' => 1, 'name' => 'Ari',    'email' => 'ariwiraasmara.sc37@gmail.com',  'password' => myfunction::encrypt('123456789'), 'foto' => null],
            ['id' => 2, 'name' => 'Bilqis', 'email' => 'putribilqis60@gmail.com',       'password' => myfunction::encrypt('123456789'), 'foto' => null],
            ['id' => 3, 'name' => 'Ida',    'email' => 'idafaridacantika@gmail.com',    'password' => myfunction::encrypt('123456789'), 'foto' => null],
        ];
        User::insert($users);

        $rooms = [
            ['id1004' => 'TELA@ROOM001', 'room' =>  1, 'belongto' => 'wiraasmara', 'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM002', 'room' =>  2, 'belongto' => 'wiraasmara', 'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM003', 'room' =>  3, 'belongto' => 'wiraasmara', 'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM004', 'room' =>  4, 'belongto' => 'wiraasmara', 'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM005', 'room' =>  5, 'belongto' => 'wiraasmara', 'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM006', 'room' =>  6, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM007', 'room' =>  7, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM008', 'room' =>  8, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM009', 'room' =>  9, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM010', 'room' => 10, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM011', 'room' => 11, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM012', 'room' => 12, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM013', 'room' => 13, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM014', 'room' => 14, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
            ['id1004' => 'TELA@ROOM015', 'room' => 15, 'belongto' => 'ida',        'is_active' => 0, 'keterangan' => null],
        ];
        astela_1004_room::insert($rooms);
    }
}
