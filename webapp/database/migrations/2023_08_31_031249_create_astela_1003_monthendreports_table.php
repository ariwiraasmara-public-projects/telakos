<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('astela_1003_monthendreport', function (Blueprint $table) {
            $table->string('id1003')->primary();
            $table->date('date');
            $table->text('keterangan');
            $table->float('jumlah', 255, 2);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('astela_1003_monthendreport');
    }
};
