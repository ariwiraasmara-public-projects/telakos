<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('astela_1002_tenant', function (Blueprint $table) {
            $table->string('id1002')->primary();
            $table->string('id1004')->nullable()->default(null);
            $table->string('nama');
            $table->string('tlp')->nullable()->default(null);
            $table->text('alamat')->nullable()->default(null);
            $table->date('tgl_bayar');
            $table->float('jumlah', 255, 2)->nullable()->default(0);
            $table->enum('tipe', [1, 6, 12])->nullable()->default(null);
            $table->text('sk')->nullable()->default(null);
            $table->integer('is_active')->nullable()->default(0);
            $table->text('keterangan')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('astela_1002_tenant');
    }
};
