<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('astela_1004_room', function (Blueprint $table) {
            $table->string('id1004')->primary();
            $table->integer('room')->default(0);
            $table->enum('belongto', ['wiraasmara', 'ida'])->nullable();
            $table->integer('is_active')->default(0);
            $table->text('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('astela_1004_room');
    }
};
