<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('astela_1005_endoflife', function (Blueprint $table) {
            $table->string('id1002')->primary();
            $table->string('id1003')->nullable()->default(null);
            $table->date('tgl_bayar');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('astela_1005_endoflife');
    }
};
