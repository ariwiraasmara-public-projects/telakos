import { useState, Fragment } from 'react';
import { styled } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import Fab from '@mui/material/Fab';
import IconButton from '@mui/material/IconButton';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import AssignmentTurnedInIcon from '@mui/icons-material/AssignmentTurnedIn';
import BedroomChildIcon from '@mui/icons-material/BedroomChild';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import HomeIcon from '@mui/icons-material/Home';
import LogoutIcon from '@mui/icons-material/Logout';

const StyledFab = styled(Fab)({
    position: 'absolute',
    zIndex: 1,
    top: -30,
    left: 0,
    right: 0,
    margin: 'auto',
});

export default function AuthenticatedLayout(props) {

    function gotoAddPage($page) {
        window.location.href = $page;
    }

    return (
        <Fragment>
            <AppBar position="fixed" color="primary" sx={{ top: 0, bottom: 'auto'}}>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    {props.title}
                </Typography>
                {props.isadd ? (
                    <div>
                        <IconButton
                            size="large"
                            onClick={() => gotoAddPage(props.urladd)}
                            color="inherit"
                        >
                            <AddCircleOutlineIcon />
                        </IconButton>
                    </div>
                ) : (null)}
            </AppBar>
            <Paper square sx={{ pb: '50px' }}>
                {props.children}
            </Paper>
            <AppBar position="fixed" color="primary" sx={{ top: 'auto', bottom: 0 }}>
                <BottomNavigation
                    sx={{ background: 'none', color: 'white' }}
                    showLabels
                    value={value}
                    onChange={(event, newValue) => {
                        setValue(newValue);
                    }}
                >
                    <BottomNavigationAction
                        sx={{ color: 'white' }}
                        label="Kamar"
                        icon={<BedroomChildIcon />} />

                    <BottomNavigationAction
                        sx={{ color: 'white' }}
                        label="Penyewa"
                        icon={<FormatListBulletedIcon />} />

                    <StyledFab color="secondary" label="Dashboard">
                        <HomeIcon />
                    </StyledFab>

                    <BottomNavigationAction
                        sx={{ color: 'white' }}
                        label="Laporan"
                        icon={<AssignmentTurnedInIcon />} />

                    <BottomNavigationAction
                        sx={{ color: 'white' }}
                        label="Profil"
                        icon={<AccountCircleIcon />} />
                </BottomNavigation>
            </AppBar>
        </Fragment>
    );
}
