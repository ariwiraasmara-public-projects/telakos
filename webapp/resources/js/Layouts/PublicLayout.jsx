import { useState, Fragment } from 'react';
import AppBar from '@mui/material/AppBar';
export default function PublicLayout(props) {
    return(
        <Fragment>
            <AppBar position="fixed" color="primary" sx={{ top: 0, bottom: 'auto'}}>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    {props.title}
                </Typography>
            </AppBar>
            {children}
        </Fragment>
    );
}