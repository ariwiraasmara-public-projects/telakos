import './unauthorizedlayout.css'
import { Head } from '@inertiajs/react';
import Box from '@mui/material/Box';
export default function Unauthorized(props) {
    return(
        <Box sx={{ m: 'auto' }}>
            <Head title="Unauthorized!" />

            <div className="unauthorized">
                {props.children}
            </div>
        </Box>
    );
}
