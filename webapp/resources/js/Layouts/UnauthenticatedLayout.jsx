import './unauthenticatedlayout.css';
import { useState } from 'react';

export default function UnauthenticatedLayout(props) {
    return (
        <main>{props.children}</main>
    );
}
