import Layout from '../Layouts/UnauthenticatedLayout';
import { Head } from '@inertiajs/react';
import { submitLogin } from '../External_Function/api';
import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import AppRegistrationIcon from '@mui/icons-material/AppRegistration';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import DoneIcon from '@mui/icons-material/Done';
import InfoIcon from '@mui/icons-material/Info';
import LoginIcon from '@mui/icons-material/Login';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import TextField  from '@mui/material/TextField';

const style = {
    p: 2,
    m: 'auto',
    color: '#fff',
}

export default function Login(props) {
    // const logedin = document.location.href = "/teh/dashboard";
    function toRegister() {
        window.location.href = "/teh/register";
    }

    function toForgotpassword() {
        window.location.href = "/teh/forgotpassword";
    }

    const [email, setEmail] = useState('');
    const onEmail = (event) => {
        setEmail(event.target.value);
    }

    const [password, setPassword] = useState('');
    const onPassword = (event) => {
        setPassword(event.target.value);
    }

    const [rememberme, setRememberme] = useState(0);
    const onRememberme = (event) => {
        if(event.target.checked) setRememberme(99999);
        else setRememberme(0);
    }

    async function handleLogin(email, password) {
        const result = await submitLogin(email, password);
        if(result.data.success == 1) {
            Cookies.set('islogin', 1, { expires: rememberme, path: '/' }),
            Cookies.set('email', response.data.data.email, { expires: rememberme, path: '/' }),
            window.location.href = "/teh/dashboard";
        }
        else {
            console.log('error', result);
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                text: 'Terjadi Kesalahan! Silahkan Coba Lagi!',
                confirmButtonText: 'OK',
            })
        }
    }

    useEffect(() => {
        // if(props.islogin == 1) setTimeout(logedin, 5000);
    }, []);

    return(
        props.islogin > 0 ? (
            <div sx={{ m: 'auto' }}>
                <h1 className="title-h1">Should Not Be Here!</h1>
            </div>
        ) : (
            <Layout>
                <Head title="Login" />

                <div sx={style} className="boxbf text-is-white">
                    <h1 className="center title-h1">Login</h1>
                    <div className="m-t-30">
                        <TextField  sx={{ color: 'white' }} type="text"
                                    fullWidth label="Email" id="email"
                                    variant="outlined"
                                    value={email} onChange={onEmail} />

                        <TextField  sx={{ color: '#fff' }} type="password"
                                    fullWidth label="Password" id="password"
                                    variant="outlined" className="m-t-10"
                                    value={password} onChange={onPassword} />

                        <Grid container spacing={3} className="m-t-0">
                            <Grid item xs={6} md={6} xl={6}>
                                <FormControlLabel control={<Checkbox />}
                                                  label="Remember Me"
                                                  value={rememberme}
                                                  onChange={onRememberme} />
                            </Grid>

                            <Grid item xs={6} md={6} xl={6} className="m-t-10 right">
                                <a href="#forgotpassword" onClick={() => toForgotpassword}>Forgot Password</a>
                            </Grid>
                        </Grid>

                        <Grid container className="center m-t-10">
                            <Grid item xs={6} md={6} xl={6} sx={{p:1}}>
                                <Button type="button" fullWidth variant="contained" size="large"
                                        onClick={handleLogin(email, password)} >
                                    <DoneIcon />
                                </Button>
                            </Grid>

                            <Grid item xs={6} md={6} xl={6} sx={{p:1}}>
                                <Button type="button" fullWidth variant="contained" size="large"
                                        onClick={toRegister}>
                                    <AppRegistrationIcon />
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </Layout>
        )
    );
}
