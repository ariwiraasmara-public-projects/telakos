import { Head } from '@inertiajs/react';
import { getTenant } from '../Extenal_Function/api.js';
import { currency, format_datetime } from '../External_Function/myfunction.js';
import React, { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

export default function ViewTenant(props) {

    const [directory, setDirectory] = useState('');
    const [kamar, setKamar] = useState(null);
    const [nama, setNama] = useState('');
    const [tlp, setTlp] = useState('');
    const [tanggalbayar, setTanggalbayar] = useState('');
    const [tanggalbatasakhir, setTanggalbatasakhir] = useState('');
    const [jumlah, setJumlah] = useState('');
    const [tipe, setTipe] = useState(0);
    const [sk, setSk] = useState(0);
    const [penyewaaktif, setPenyewaaktif] = useState(0);
    const [keteranganpenyewa, setKeteranganpenyewa] = useState(0);

    async function getTenant(id) {
        const result = await getTenant(id);
        if(result.data.success == 1) {
            setDirectory(result.data.directory);
            setKamar(result.data.data.kamar);
            setNama(result.data.data.nama);
            setTlp(result.data.data.tlp);
            setTanggalbayar(result.data.data.tanggalbayar);
            setTanggalbatasakhir(result.data.data.tanggalbatasakhir);
            setJumlah(result.data.data.jumlah);
            setTipe(result.data.data.tipe);
            setSk(result.data.data.sk);
            setPenyewaaktif(result.data.data.penyewaaktif);
            setKeteranganpenyewa(result.data.data.keteranganpenyewa);
        }
    }

    useEffect(() => {
        getTenant();
    }, []);

    return(
        <Box sx={{ p: 2 }}>
            <Head title={`Kwitansi ${nama}`} />
            <Grid container spacing={3} className="row-line">
                <Grid item xs={6} sm={6} lg={6}>
                    <span className="bold">Aktif ?</span>
                </Grid>

                <Grid item xs={6} sm={6} lg={6}>
                    <div className="right">
                        {penyewaaktif > 0 ? 'Ya' : 'Tidak'}
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={3} className="row-line">
                <Grid item xs={6} sm={6} lg={6}>
                    <span className="bold">Nama</span>
                </Grid>

                <Grid item xs={6} sm={6} lg={6}>
                    <div className="right">
                        {nama}
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={3} className="row-line">
                <Grid item xs={6} sm={6} lg={6}>
                    <span className="bold">Kamar Nomor</span>
                </Grid>

                <Grid item xs={6} sm={6} lg={6}>
                    <div className="right">
                        {kamar}
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={3} className="row-line">
                <Grid item xs={6} sm={6} lg={6}>
                    <span className="bold">No. Telepon</span>
                </Grid>

                <Grid item xs={6} sm={6} lg={6}>
                    <div className="right">
                        {tlp}
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={3} className="row-line">
                <Grid item xs={6} sm={6} lg={6}>
                    <span className="bold">Tanggal Bayar</span>
                </Grid>

                <Grid item xs={6} sm={6} lg={6}>
                    <div className="right">
                        {format_datetime(tanggalbayar, true)}
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={3} className="row-line">
                <Grid item xs={6} sm={6} lg={6}>
                    <span className="bold">Jumlah</span>
                </Grid>

                <Grid item xs={6} sm={6} lg={6}>
                    <div className="right">
                        {currency(jumlah, 'id-ID', 'IDR')}
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={3} className="row-line">
                <Grid item xs={6} sm={6} lg={6}>
                    <span className="bold">Tipe</span>
                </Grid>

                <Grid item xs={6} sm={6} lg={6}>
                    <div className="right">
                        {tipe} Bulan
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={3} className="row-line">
                <Grid item xs={6} sm={6} lg={6}>
                    <span className="bold">Sampai Dengan</span>
                </Grid>

                <Grid item xs={6} sm={6} lg={6}>
                    <div className="right">
                        {format_datetime(sampaidengan, true)}
                    </div>
                </Grid>
            </Grid>

            <Grid container spacing={3} className="row-line">
                <Grid item xs={6} sm={6} lg={6}>
                    <span className="bold">Keterangan</span>
                </Grid>

                <Grid item xs={6} sm={6} lg={6}>
                    <div className="right">
                        {keteranganpenyewa}
                    </div>
                </Grid>
            </Grid>

            <div className="m-t-10">
                <span className="bold">Surat Kontrak</span>
                <img src={directory+sk} alt={`Surat Kontrak ${nama}`} />
            </div>
        </Box>
    );
}
