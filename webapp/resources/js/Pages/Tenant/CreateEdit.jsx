import dayjs from 'dayjs';
import AuthLayout from '../Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';
import { storeTenant, getTenant, updateTenant, updateTenantActive } from '../Extenal_Function/api.js';
import React, { useState } from 'react';

import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

import Link from '@mui/material/Link';
import Button from '@mui/material/Button';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Switch from '@mui/material/Switch';
import Textfield from '@mui/material/Textfield';

import DoneIcon from '@mui/icons-material/Done';

export function CreateEditTenant(props) {
    const logedout = document.location.href = "/teh/login";

    const [disabled, setDisabled] = useState(false);

    const date = new Date();
    const [tanggalbayar, setTanggalbayar] = useState(`${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`);
    const onTanggalbayar = (event) => {
        setTanggalbayar(event.target.value);
    }

    const [kamar, setKamar] = useState(0);
    const onKamar = (event) => {
        setKamar(event.target.value);
    };

    const [nama, setNama] = useState('');
    const onNama = (event) => {
        setNama(event.target.value);
    }

    const [tlp, setTlp] = useState('');
    const onTlp = (event) => {
        setTlp(event.target.value);
    }

    const [jumlah, setJumlah] = useState(0);
    const onJumlah = (event) => {
        setJumlah(event.target.value);
    }

    const [tipe, setTipe] = useState(0);
    const onTipe = (event) => {
        setTipe(event.target.value);
    }

    const [tanggalbatasakhir, setTanggalbatasakhir] = useState('');

    const [sk, setSk] = useState(null);
    const onSk = (event) => {
        setSk(event.target.value);
    }

    const [keterangan, setKeterangan] = useState('');
    const onKeterangan = (event) => {
        setKeterangan(event.target.value);
    }

    function handleSubmitTenant(kamar, nama, tlp, tgl_bayar, jumlah, tipe, sk, keterangan) {
        const result = storeTenant(room, nama, tlp, tgl_bayar, jumlah, tipe, sk, keterangan);
        if(result.data.success == 1) {
            Swal.fire({
                title: 'Sukses!',
                icon: 'success',
                text: 'Sistem Berhasil Menyimpan Data Anda!',
                confirmButtonText: 'OK',
            })
        }
        else {
            console.log('error', result);
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                text: 'Terjadi Kesalahan! Silahkan Coba Lagi!',
                confirmButtonText: 'OK',
            })
        }
    }

    async function getTenant(idReq) {
        const result = await getTenant(idReq);
        if(result.data.success == 1) {
            setDisabled(true);
            setKamar(result.data.data.kamar);
            setNama(result.data.data.nama);
            setTlp(result.data.data.tlp);
            setTanggalbayar(result.data.data.tanggalbayar);
            setTanggalbatasakhir(result.data.data.tanggalbatasakhir);
            setJumlah(result.data.data.jumlah);
            setTipe(result.data.data.tipe);
            setSk(result.data.data.sk);
            setPenyewaaktif(result.data.data.penyewaaktif);
            setKeteranganpenyewa(result.data.data.keteranganpenyewa);
        }
    }

    async function updateTenant(idReq, tgl_bayarReq, jumlahReq, tipeReq, keteranganReq) {
        const result = await updateTenant(idReq, tgl_bayarReq, jumlahReq, tipeReq, keteranganReq);
        if(result.data.success == 1) {
            Swal.fire({
                title: 'Sukses!',
                icon: 'success',
                text: 'Sistem Berhasil Menyimpan Data Anda!',
                confirmButtonText: 'OK',
            })
        }
        else {
            console.log('error', result);
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                text: 'Terjadi Kesalahan! Silahkan Coba Lagi!',
                confirmButtonText: 'OK',
            })
        }
    }

    async function updateTenantActive(idReq, activeReq) {
        const result = await updateTenantActive(idReq, activeReq);
        if(result.data.success == 1) {
            Swal.fire({
                title: 'Sukses!',
                icon: 'success',
                text: 'Sistem Berhasil Menyimpan Data Anda!',
                confirmButtonText: 'OK',
            })
        }
        else {
            console.log('error', result);
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                text: 'Terjadi Kesalahan! Silahkan Coba Lagi!',
                confirmButtonText: 'OK',
            })
        }
    }

    const [aktif, setAktif] = useState(1);
    const onAktif = (event) => {
        if(even.target.value) {
            setAktif(1);
            updateTenantActive(props.id, aktif);
        }
        else {
            setAktif(0);
            updateTenantActive(props.id, aktif);
        }
    };

    useEffect(() => {
        if(props.islogin < 1) setTimeout(logedout, 5000);
        if(props.isedit == 1) getTenant();
    }, []);

    return(
        props.islogin < 1 ? (
            <div sx={{ m: 'auto' }}>
                <h1 className="title bold">
                    Unauthorized!<br/>
                    Permission Denied to Access this Page!<br/>
                    GET OUT!
                </h1>
            </div>
        ) : (
            <AuthLayout title="Tambah Penyewa Baru">
                <Head title="Tambah Penyewa Baru" />

                {props.isedit == 1 ? (
                    <FormGroup>
                        <FormControlLabel control={<Switch value={aktif} onChange={() => onAktif} defaultChecked />} label="Aktif" />
                    </FormGroup>
                ) : (null)}

                <TextField  fullWidth id="create-nama" placeholder="Nama"
                            variant="outlined"
                            value={nama} onChange={onNama} />

                <Select
                    fullWidth
                    value={kamar}
                    onChange={onKamar}
                    displayEmpty
                    className="m-t-10"
                    inputProps={{ 'aria-label': 'Kamar No' }}
                >
                    <MenuItem value="" selected disabled>
                        <em>Pilih Kamar</em>
                    </MenuItem>
                    {props.listkamar > 0 ? (
                        props.listkamar.map((body, index)=>(
                            <MenuItem value={body.id1004}>{body.room}</MenuItem>
                        ))
                    ) : (
                        <MenuItem value="" selected disabled>
                            <em>Kamar Penuh</em>
                        </MenuItem>
                    ) }
                </Select>

                <TextField  fullWidth id="create-tlp" placeholder="No. Telepon"
                            variant="outlined" className="m-t-10"
                            value={tlp} onChange={onTlp} />

                <LocalizationProvider fullWidth dateAdapter={AdapterDayjs} className="m-t-10">
                    <DemoContainer components={["DatePicker"]}>
                        <DatePicker label="Tanggal Bayar" value={tanggalbayar} defaultValue={dayjs(tanggalbayar)} onChange={onTanggalbayar} />
                    </DemoContainer>
                </LocalizationProvider>

                <TextField  fullWidth inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                            id="create-jumlah" placeholder="Jumlah"
                            variant="outlined" className="m-t-10"
                            value={jumlah} onChange={onJumlah} />

                <div className="m-t-10" fullWidth>
                    <FormControl>
                        <span id="demo-radio-buttons-group-label">Tipe</span>
                        <RadioGroup
                            row
                            aria-labelledby="demo-radio-buttons-group-label"
                            defaultValue={tipe}
                            name="radio-buttons-group"
                            onChange={onTipe}
                        >
                            <FormControlLabel value="1" control={<Radio />} label="1 Bulan" />
                            <FormControlLabel value="6" control={<Radio />} label="6 Bulan" />
                            <FormControlLabel value="12" control={<Radio />} label="12 Bulan" />
                        </RadioGroup>
                    </FormControl>
                </div>

                {props.isedit == 1 ? (
                    <div className="m-t-10" fullWidth>
                        <span className="bold">Sampai Dengan :</span> <span>{tanggalbatasakhir}</span>
                    </div>
                ) : (null)}

                <TextField  fullWidth focused
                            type="file" id="create-sk" label="Surat Kontrak"
                            variant="outlined" className="m-t-10"
                            value={sk} onChange={onSk} disabled={disabled} />

                <TextField  id="create-keterangan" placeholder="Keterangan"
                            variant="outlined"  className="m-t-10"
                            value={keterangan} onChange={onKeterangan} />

                <Button type="button" fullWidth variant="contained" size="large"
                        onClick={() => props.isedit == 1 ? updateTenant(props.id, tanggalbayar, jumlah, tipe, keterangan) : handleSubmitTenant(kamar, nama, tlp, tgl_bayar, jumlah, tipe, sk, keterangan)} >
                    <DoneIcon />
                </Button>

            </AuthLayout>
        )
    );
}
