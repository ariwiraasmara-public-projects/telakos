import AuthLayout from '../Layouts/AuthenticatedLayout';
import PublicLayout from '../Layouts/PublicLayout';
import { Head } from '@inertiajs/react';
import { allTenant } from '../Extenal_Function/api.js';
import React, { useState, useEffect, Fragment } from 'react';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import Textfield from '@mui/material/Textfield';

export default function AllTenant(props) {

    const [Pagelayout, setPagelayout] = useState(null);
    function setupLayout(param) {
        if(param) setPagelayout(<AuthLayout title={'Semua Penyewa'} isadd={true} urladd={'teh/tenant/create'} />);
        else setPagelayout(<PublicLayout title={'Semua Penyewa'} />);
    }

    const [alltenant, setAlltenant] = useState([]);
    async function getAllTenant() {
        const result = await allTenant('date', 'desc');
        if(result.data.success == 1) setAlltenant(result.data.data);
        else setAlltenant('__none__');
    }

    const [find, setFind] = useState('');
    const findTenant = (event) => {
        setFind(event.target.value);
        if(find == '' || find == null) getAllTenant();
        // else setAllreport(); // Jika nilai tidak sama dengan null, maka nilai pada allreport berubah sesuai dengan yang dicari
    }

    useEffect(() => {
        setupLayout(props.islogin);
        getAllTenant()
    }, []);

    return(
        <Pagelayout>
            <Head title={'Semua Penyewa'} />

            {alltenant == '__none__' ? (
                <h1 sx={{ m: 'auto' }} className="title bold">Tidak Ada Data Penyewa</h1>
            ) : (
                <Fragment>
                    <TextField id="find-tenant" label="Outlined" variant="outlined" value={find} onChange={findTenant} />
                    {alltenant.map((body, index) => (
                        <Grid container spacing={3}>
                            <Link href={`/teh/tenant/${body.id1002}`}>
                                <Grid item xs={6} sm={4} lg={2}>
                                    <span className="">{body.nama}</span>
                                    {props.islogin > 0 ? (
                                        <Fragment>
                                            <span className="">{body.kamar}</span><br/>
                                            <span className="">{body.tlp}</span><br/>
                                            <span className="">{body.penyewa_aktif}</span>
                                        </Fragment>
                                    ) : (null)}
                                </Grid>
                            </Link>
                        </Grid>
                    ))}
                </Fragment>
            )}
        </Pagelayout>
    );
}
