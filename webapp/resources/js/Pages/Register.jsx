import Layout from '../Layouts/UnauthenticatedLayout';
import { Head } from '@inertiajs/react';
import { submitRegister } from '../External_Function/api.js';
import React, { useState, useEffect } from 'react';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Button from '@mui/material/Button';
import DoneIcon from '@mui/icons-material/Done';
import Grid from '@mui/material/Grid';
import TextField  from '@mui/material/TextField';

const style = {
    p: 2,
    m: 'auto',
    color: '#fff',
}

export default function Register(props) {
    // const logedin = document.location.href = "/teh/dashboard";
    function toLogin() {
        window.location.href = "/teh/login";
    }

    const [name, setName] = useState('');
    const onName = (event) => {
        setName(event.target.value);
    }

    const [email, setEmail] = useState('');
    const onEmail = (event) => {
        setEmail(event.target.value);
    }

    const [password, setPassword] = useState('');
    const onPassword = (event) => {
        setPassword(event.target.value);
    }

    async function handleRegister(name, email, password) {
        const result = await register(name, email, password);
        if(result.data.success == 1) {
            Swal.fire({
                title: 'Sukses!',
                icon: 'success',
                text: 'Sistem Berhasil Menyimpan Data Anda! Anda Sudah Bisa Login Dengan Data Yang Telah Dimasukkan',
                confirmButtonText: 'OK',
            })
        }
        else {
            console.log('error', result);
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                text: 'Terjadi Kesalahan! Silahkan Coba Lagi!',
                confirmButtonText: 'OK',
            })
        }
    }

    useEffect(() => {
        // if(props.islogin == 1) setTimeout(logedin, 5000);
    }, []);

    return(
        props.islogin > 0 ? (
            <div sx={{ m: 'auto' }}>
                <h1 className="title-h1">Should Not Be Here!</h1>
            </div>
        ) : (
            <Layout>
                <Head title="Register" />

                <div sx={style} className="boxbf text-is-white">
                    <h1 className="center title-h1">Daftar</h1>
                    <div className="m-t-30">
                        <TextField  sx={{ color: 'white' }} type="text"
                                    fullWidth label="Nama" id="name"
                                    variant="outlined"
                                    value={name} onChange={onName} />

                        <TextField  sx={{ color: 'white' }} type="text"
                                    fullWidth label="Email" id="email"
                                    variant="outlined" className="m-t-10"
                                    value={email} onChange={onEmail} />

                        <TextField  sx={{ color: 'white' }} type="password"
                                    fullWidth label="Password" id="password"
                                    variant="outlined" className="m-t-10"
                                    value={password} onChange={onPassword} />


                        <Grid container className="center m-t-10">
                            <Grid item xs={6} md={6} xl={6} sx={{p:1}}>
                                <Button type="button" fullWidth variant="contained" size="large"
                                        onClick={toLogin} >
                                    <ArrowBackIcon />
                                </Button>
                            </Grid>

                            <Grid item xs={6} md={6} xl={6} sx={{p:1}}>
                                <Button type="button" fullWidth variant="contained" size="large"
                                        onClick={handleRegister(name, email, password)} >
                                    <DoneIcon />
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </Layout>
        )
    );
}
