import Layout from '../Layouts/AuthenticatedLayout';
import Unauthorized from '../Layouts/UnauthorizedLayout';
import { Head } from '@inertiajs/react';
import { getDashboard } from '../External_Function/api';
import React, { useState, useEffect } from 'react';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';

export default function Dashboard(props) {
    const [recenttenant, setRecenttenant] = useState([]);
    const [recentreport, setRecentreport] = useState([]);

    async function getDashboard() {
        const result = await getDashboard;
        if(result.data.success == 1) {
            setRecenttenant(result.data.data.recent_tenant);
            setRecentreport(result.data.data.recent_report);
        }
    }

    useEffect(() => {
        if(props.islogin != 1) setTimeout(document.location.href = "/teh/login", 30000);
        getDashboard();
    }, []);

    return(
        props.islogin < 1 ? (
            <Unauthorized>
                Unauthorized!<br/>
                Permission Denied to Access this Page!<br/>
                GET OUT!
            </Unauthorized>
        ) : (
            <Layout title={props.title}>
                <Head title={props.title} />

                <h3 className="title">Penyewa Terbaru</h3>
                <div className="m-t-30">
                    {recenttenant == '__none__' ? (
                        <span className="bold">Tidak Ada Data</span>
                    ) : (
                        recenttenant.map((body, index) => (
                            <Grid container spacing={3}>
                                <Link href={`/teh/penyewa/${body.id1002}/${islogin}`}>
                                    <Grid item xs={6} sm={4} lg={2}>
                                        <span className="">{body.nama}</span>
                                    </Grid>
                                </Link>
                            </Grid>
                        ))
                    )}
                </div>

                <h3 className="title m-t-50">Laporan Terbaru</h3>
                <div className="m-t-30">
                    {recentreport == '__none__' ? (
                        <span className="bold">Tidak Ada Data</span>
                    ) : (
                        recentreport.map((body, index) => (
                            <Grid container spacing={3}>
                                <Link href={`/teh/laporan/${body.end_day_of_month}`}>
                                    <Grid item xs={6} sm={4} lg={2}>
                                        <span className="">{body.bulan}</span>
                                    </Grid>
                                </Link>
                            </Grid>
                        ))
                    )}
                </div>
            </Layout>
        )

    );
}
