import AuthLayout from '../Layouts/AuthenticatedLayout';
import PublicLayout from '../Layouts/PublicLayout';
import { Head } from '@inertiajs/react';
import { allReport } from '../Extenal_Function/api.js';
import React, { useState, useEffect, Fragment } from 'react';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import Textfield from '@mui/material/Textfield';

export default function AllReport(props) {

    const [Pagelayout, setPagelayout] = useState(null);
    function setupLayout(param) {
        if(param) setPagelayout(<AuthLayout title={'Semua Laporan'} isadd={true} urladd={'teh/report/create'} />);
        else setPagelayout(<PublicLayout title={'Semua Laporan'} />);
    }

    const [allreport, setAllreport] = useState([]);
    async function getAllReport(asc) {
        const result = await allReport(asc);
        if(result.data.success == 1) setAllreport(result.data.data);
        else setAllreport('__none__');
    }

    const [find, setFind] = useState('');
    const findReport = (event) => {
        setFind(event.target.value);
        if(find == '' || find == null) setAllreport();
        // else setAllreport(); // Jika nilai tidak sama dengan null, maka nilai pada allreport berubah sesuai dengan yang dicari
    }

    useEffect(() => {
        setupLayout(props.islogin);
        getAllReport('desc')
    }, []);

    return(
        <PageLayout>
            <Head title="Semua Penyewa" />

            {allreport == '__none__' ? (
                <h1 sx={{ m: 'auto' }} className="title bold">Tidak Ada Data Laporan</h1>
            ) : (
                <Fragment>
                    <Select
                        fullWidth
                        value={kamar}
                        onChange={onKamar}
                        displayEmpty
                        className="m-t-10"
                        inputProps={{ 'aria-label': 'Kamar No' }}
                    >
                        <MenuItem value="" selected disabled>
                            <em>Pilih Kamar</em>
                        </MenuItem>
                        {props.listkamar > 0 ? (
                            props.listkamar.map((body, index)=>(
                                <MenuItem value={body.id1004}>{body.room}</MenuItem>
                            ))
                        ) : (
                            <MenuItem value="" selected disabled>
                                <em>Kamar Penuh</em>
                            </MenuItem>
                        ) }
                    </Select>

                    {allreport.map((body, index) => (
                        <Grid container spacing={3}>
                            <Link href={`/teh/laporan/${body.end_day_of_month}`}>
                                <Grid item xs={6} sm={4} lg={2}>
                                    <span className="">{body.bulan}</span>
                                </Grid>
                            </Link>
                        </Grid>
                    ))}
                </Fragment>
            )}
        </PageLayout>
    );
}
