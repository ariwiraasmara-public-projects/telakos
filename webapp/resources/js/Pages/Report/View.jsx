import { Head } from '@inertiajs/react';
import { viewReport } from '../Extenal_Function/api.js';
import React, { useState, useEffect, Fragment } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { currency } from '@/External_Function/myfunction';

export default function ViewReport(props) {

    const [tenant, setTenant] = useState([]);
    const [tenanttotal, setTenanttotal] = useState(0);
    const [tenanteachtotal, setTenanteachtotal] = useState([]);
    const [detail, setDetail] = useState([]);
    const [detailtotal, setDetailtotal] = useState(0);

    async function getReport(dateReq) {
        const result = await viewReport(dateReq);
        if(result.data.success == 1) {
            setTenant(result.data.tenant.data);
            setTenanteachtotal(result.data.tenant.each_total);
            setTenanttotal(result.data.tenant.total_in_a_month);
            setDetail(result.data.detail.data);
            setDetailtotal(result.data.detail.total);
        }
    }

    useEffect(() => {
        getReport();
    }, []);

    return(
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: 440 }}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center" style={{ minWidth: 10 }} rowspan="2">
                                No.
                            </TableCell>

                            <TableCell align="center" style={{ minWidth: 50 }} rowspan="2">
                                Nama
                            </TableCell>

                            <TableCell align="center" style={{ minWidth: 10 }} rowspan="2">
                                No. HP
                            </TableCell>

                            <TableCell align="center" style={{ minWidth: 50 }} rowspan="2">
                                Alamat
                            </TableCell>

                            <TableCell align="center" style={{ minWidth: 150 }} colspan="3">
                                Jumlah
                            </TableCell>

                            <TableCell align="center" style={{ minWidth: 50 }} rowspan="2">
                                Jumlah Total
                            </TableCell>

                            <TableCell align="center" style={{ minWidth: 100 }} rowspan="2">
                                Keterangan
                            </TableCell>
                        </TableRow>

                        <TableRow>
                            <TableCell align="center" style={{ minWidth: 50 }}>
                                1 Bulan
                            </TableCell>

                            <TableCell align="center" style={{ minWidth: 50 }}>
                                6 Bulan
                            </TableCell>

                            <TableCell align="center" style={{ minWidth: 50 }}>
                                12 Bulan
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {tenant == '__none__' ? (
                            <TableRow hover>
                                <TableCell colspan="9"><h1 className="bold">Tidak Ada Data</h1></TableCell>
                            </TableRow>
                        ) : (
                            tenant.map((row, index) => (
                                <TableRow hover key={row}>
                                    <TableCell align="right">{index+1}</TableCell>
                                    <TableCell align="left">{row.nama}</TableCell>
                                    <TableCell align="center">{row.tlp}</TableCell>
                                    <TableCell align="left">{row.alamat}</TableCell>
                                    <TableCell align="right">
                                        {row.tipe == 1 ? (row.jumlah) : (0)}
                                    </TableCell>
                                    <TableCell align="right">
                                        {row.tipe == 6 ? (row.jumlah) : (0)}
                                    </TableCell>
                                    <TableCell align="right">
                                        {row.tipe == 12 ? (row.jumlah) : (0)}
                                    </TableCell>
                                    <TableCell align="right">{row.jumlah}</TableCell>
                                    <TableCell align="left">{row.keterangan}</TableCell>
                                </TableRow>
                            ))
                        )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TableCell align="center" colspan="4">
                                <h1 className="bold">Total</h1>
                            </TableCell>
                            {tenanteachtotal == '__none__' ? (
                                <Fragment>
                                    <TableCell align="right" colspan="3">{currency('0', 'id-ID', 'IDR')}</TableCell>
                                    <TableCell align="right">{currency('0', 'id-ID', 'IDR')}</TableCell>
                                    <TableCell align="center">-</TableCell>
                                </Fragment>

                            ) : (
                                tenanteachtotal.map((row, index) => (
                                    <Fragment>
                                        <TableCell>{currency(row.total_1_bulan, 'id-ID', 'IDR')}</TableCell>
                                        <TableCell>{currency(row.total_6_bulan, 'id-ID', 'IDR')}</TableCell>
                                        <TableCell>{currency(row.total_12_bulan, 'id-ID', 'IDR')}</TableCell>
                                    </Fragment>

                                ))
                            )}
                        </TableRow>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell colspan="6">Keterangan</TableCell>
                        </TableRow>
                        {detail == '__none__' ? (
                            <TableRow>
                                <TableCell colspan="9"><h1 className="bold">Tidak Ada Data</h1></TableCell>
                            </TableRow>
                        ) : (
                            detail.map((row, index) => (
                                <TableRow>
                                    <TableCell align="right">{index+1}</TableCell>
                                    <TableCell>{row.keterangan}</TableCell>
                                    <TableCell>{row.jumlah}</TableCell>
                                </TableRow>
                            ))
                        )}
                    </TableFooter>
                </Table>
            </TableContainer>
        </Paper>
    );
}
