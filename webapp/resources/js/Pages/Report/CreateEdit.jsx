import AuthLayout from '../Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';
import $ from 'jquery';
import { storeReport, getReport, updateReport } from '../Extenal_Function/api.js';
import React, { useState, Fragment } from 'react';
import Button from '@mui/material/Button';
import Textfield from '@mui/material/Textfield';

import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import DoneIcon from '@mui/icons-material/Done';

function newRow() {
    return(
        <div className="row-line">
            <TextField  fullWidth id="create-deskripsi" placeholder="Deskripsi"
                    variant="outlined" className="m-t-10"
                    value={tlp} onChange={onTlp} />

            <TextField  fullWidth id="create-jumlah" placeholder="Jumlah"
                    variant="outlined" className="m-t-10"
                    value={tlp} onChange={onTlp} />
        </div>
    );
}

export default function CreateEditReport(props) {

    const [deskripsi, setDeskripsi] = useState();
    const onDeskripsi = (event) => {
        setDeskripsi(event.target.value);
    };

    const [jumlah, setJumlah] = useState();
    const onJumlah = (event) => {
        setJumlah(event.target.value);
    };

    const handleNewRow = () => {
        $('#row-stack').append(newRow);
    }

    async function storeReport(dateReq, keteranganReq, amountReq) {
        const result = await storeReport(dateReq, keteranganReq, amountReq);
        if(result.data.success == 1) {
            Swal.fire({
                title: 'Sukses!',
                icon: 'success',
                text: 'Sistem Berhasil Menyimpan Data Anda!',
                confirmButtonText: 'OK',
            })
        }
        else {
            console.log('error', result);
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                text: 'Terjadi Kesalahan! Silahkan Coba Lagi!',
                confirmButtonText: 'OK',
            })
        }
    }

    const [editdatas, setEditdatas] = useState(0);
    async function getReport(date) {
        const result = await getReport(date);
        if(result.data.success == 1) {
            setDeskripsi(result.data.data.deskripsi);
            setJumlah(result.data.data.jumlah);
            setEditdatas(result.data.data.deskripsi.length);
        }
    }

    const rendereditdatas = [];
    function getEditDatas() {
        for(x = 0; x < editdatas; x++) {
            rendereditdatas.push(
                <Fragment>
                    <TextField  fullWidth id="create-deskripsi" placeholder="Deskripsi"
                            variant="outlined" className="m-t-10"
                            value={deskripsi} onChange={onDeskripsi} />

                    <TextField  fullWidth id="create-jumlah" placeholder="Jumlah"
                            variant="outlined" className="m-t-10"
                            value={jumlah} onChange={onJumlah} />
                </Fragment>
            );
        }

    }

    async function updateReport(idReq, keteranganReq, amountReq) {
        const result = await updateReport(idReq, dateReq, keteranganReq, amountReq);
        if(result.data.success == 1) {
            Swal.fire({
                title: 'Sukses!',
                icon: 'success',
                text: 'Sistem Berhasil Menyimpan Data Anda!',
                confirmButtonText: 'OK',
            })
        }
        else {
            console.log('error', result);
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                text: 'Terjadi Kesalahan! Silahkan Coba Lagi!',
                confirmButtonText: 'OK',
            })
        }
    }

    return(
        <AuthLayout>
            <Head title="Tambah Detil Laporan Baru" />

            {props.isedit == 1 ? (
                rendereditdatas
            ) : (
                <Fragment>
                    <TextField  fullWidth id="create-deskripsi" placeholder="Deskripsi"
                            variant="outlined" className="m-t-10"
                            value={deskripsi} onChange={onDeskripsi} />

                    <TextField  fullWidth id="create-jumlah" placeholder="Jumlah"
                            variant="outlined" className="m-t-10"
                            value={jumlah} onChange={onJumlah} />
                </Fragment>
            )}

            <div id="row-stack"></div>

            <Button type="button" variant="contained" size="large"
                    sx={{ m: 'auto' }} className="m-t-10"
                    onClick={() => handleNewRow} >
                <AddCircleOutlineIcon />
            </Button>

            <Button type="button" fullWidth variant="contained" size="large"
                    onClick={() => props.isedit == 1 ? updateReport(props.id, keterangan, jumlah) : storeReport(keterangan, jumlah)} >
                <DoneIcon />
            </Button>

        </AuthLayout>
    );
}
