export function escapeHtml(text) {
    var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

export function readableHTML(text) {
    var entities = [
        ['amp', '&'],
        ['apos', '\''],
        ['#x27', '\''],
        ['#x2F', '/'],
        ['#39', '\''],
        ['#47', '/'],
        ['lt', '<'],
        ['gt', '>'],
        ['nbsp', ' '],
        ['quot', '"']
    ];

    for (var i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp('&'+entities[i][0]+';', 'g'), entities[i][1]);

    return text;
}

export function formatNumber(thenumber, koma) {
    var withkoma = thenumber.toFixed(koma);
    var parts = withkoma.toString().split(".");
    const numberPart = parts[0];
    const decimalPart = parts[1];
    const thousands = /\B(?=(\d{3})+(?!\d))/g;
    return numberPart.replace(thousands, ".") + (decimalPart ? "," + decimalPart : "");
}

export function format_datetime(dateTimeString, istime) {
    // const options = {
    //     hour12: false,
    //     year: 'numeric',
    //     month: 'long',
    //     day: 'numeric',
    //     hour: '2-digit',
    //     minute: '2-digit',
    //     second: '2-digit',
    //     timeZone: localtimezone, // Menggunakan timezone Indonesia (WIB)
    // };

    const dateTime = new Date(dateTimeString);
    // return dateTime.toLocaleDateString(localid, options);
    const nyear = dateTime.getFullYear();
    const nmonth = dateTime.getMonth();
    const ndate = dateTime.getDate();
    const nhours = dateTime.getHours()+17;
    const nminutes = dateTime.getMinutes();
    const nseconds = dateTime.getSeconds();
    const nfulldate = `${nyear}-${nmonth}-${ndate}`;
    const nfulltime = `${nhours}:${nminutes}:${nseconds}`;
    if(istime) return `${nfulldate}, ${nfulltime}`;
    else return `${nfulldate}`;
}

export function currency(thenumber, localid, currency) {
    if (typeof thenumber !== 'number') {
        // Jika thenumber bukan tipe data number, tampilkan pesan error
        console.error('data is not number');
        return thenumber;
    }

    return thenumber.toLocaleString(localid, { style: 'currency', currency: currency });
}
