const dashboard = (state = [], action) => {
    switch (action.type) {
        case "GET_DASHBOARD":
            return action.payload;
        default:
            return state;
    }
};

const tenants = (state = [], action) => {
    switch (action.type) {
        case "ALL_TENANT":
            return action.payload;
        case "CREATE_TENANT":
            return [...state, action.payload];
        case "UPDATE_TENANT":
            const index = state.findIndex((tenant) => tenant.id === action.payload.id);
            state[index] = action.payload;
            return state;
        case "DELETE_TENANT":
            return state.filter((tenant) => tenant.id !== action.payload.id);
        default:
            return state;
    }
};
