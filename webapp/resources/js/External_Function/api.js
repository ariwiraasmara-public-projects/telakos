import axios from 'axios';

const base = 'http://127.0.0.1:8000/api/';

const login = `${base}login`;
export async function submitLogin(emailReq, passReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.post(login, {
            email: emailReq,
            password: passReq,
        });
        return response;
    }
    catch (error) {
        return error;
    }
}

const register = `${base}register`;
export async function submitRegister(nameReq, emailReq, passReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.post(register, {
            name: nameReq,
            email: emailReq,
            password: passReq,
        });
        return response;
    }
    catch(err) {
        return error;
    }
}

const forgotpassword = `${base}forgotpassword`;
export async function submitForgotpassword(emailReq, passReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.post(forgotpassword, {
            email: emailReq,
            password: passReq,
        });
        return response;
    }
    catch(err) {
        return error;
    }
}

const dashboard = `${base}dashboard`;
export async function getDashboard() {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.get(dashboard);
        return response;
    }
    catch(err) {
        return error;
    }
}

const tenant = `${base}tenant`;
export async function getAllTenant(sortby, asc) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.get(`${tenant}/${sortby}/${asc}`);
        return response;
    }
    catch(err) {
        return error;
    }
}

export async function getTenant(idReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.get(`${tenant}/${idReq}`);
        return response;
    }
    catch(err) {
        return error;
    }
}

export async function storeTenant(roomReq, namaReq, tlpReq, tgl_bayarReq, jumlahReq, tipeReq, skReq, keteranganReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.post(`${tenant}/store`, {
            room: roomReq,
            nama: namaReq,
            tlp: tlpReq,
            tgl_bayar: tgl_bayarReq,
            jumlah: jumlahReq,
            tipe: tipeReq,
            sk: skReq,
            keterangan: keteranganReq
        });
        return response;
    }
    catch(err) {
        return error;
    }
}

export async function updateTenant(idReq, tgl_bayarReq, jumlahReq, tipeReq, keteranganReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.put(`${tenant}/update/${idReq}`, {
            tgl_bayar: tgl_bayarReq,
            jumlah: jumlahReq,
            tipe: tipeReq,
            keterangan: keteranganReq
        });
        return response;
    }
    catch(err) {
        return error;
    }
}

export async function updateTenantActive(idReq, activeReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.put(`${tenant}/update/active/${idReq}`, {
            is_active: activeReq
        });
        return response;
    }
    catch(err) {
        return error;
    }
}

const report = `${base}report/`;
export async function getAllReport(asc) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.get(`${report}all/${asc}`);
        return response;
    }
    catch(err) {
        return error;
    }
}

export async function getReport(date) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.get(`${report}`);
        return response;
    }
    catch(err) {
        return error;
    }
}


export async function viewReport(dateReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.get(`${report}/${dateReq}`);
        return response;
    }
    catch(err) {
        return error;
    }
}

export async function storeReport(dateReq, keteranganReq, amountReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.post(`${report}/store`, {
            date: dateReq,
            keterangan: keteranganReq,
            amount: amountReq
        });
        return response;
    }
    catch(err) {
        return error;
    }
}

export async function updateReport(idReq, dateReq, keteranganReq, amountReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.put(`${report}/update/${idReq}`, {
            date: dateReq,
            keterangan: keteranganReq,
            amount: amountReq
        });
        return response;
    }
    catch(err) {
        return error;
    }
}

export async function deleteReport(idReq) {
    try {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const response = await axios.delete(`${report}/delete/${idReq}`);
        return response;
    }
    catch(err) {
        return error;
    }
}
