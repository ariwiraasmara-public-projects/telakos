import { createStore } from 'redux';

const reducers = {
    user: (state = [], action) => {
        switch () {
            case 'GET_USER' :
                return action.payload;
            case 'CREATE_USER' :
                return [...state, action.payload];
            case 'UPDATE_USER' :
                const index = state.findIndex((tenant) => tenant.id === action.payload.id);
                state[index] = action.payload;
                return state;
            default:
                    return state;
        }
    },
    tenants: (state = [], action) => {
        switch (action.type) {
            case 'ALL_TENANT':
                return action.payload;
            case 'GET_TENANT' :
                const tenant = action.payload;
                const indexgt = state.findIndex((tenantItem) => tenantItem.id === tenant.id);
                if (indexgt !== -1) {
                    state[index] = tenant;
                }
                return state;
            case 'CREATE_TENANT':
                return [...state, action.payload];
            case 'UPDATE_TENANT':
                const indexut = state.findIndex((tenant) => tenant.id === action.payload.id);
                state[indexut] = action.payload;
                return state;
            case 'DELETE_TENANT':
                return state.filter((tenant) => tenant.id !== action.payload.id);
            default:
                return state;
        }
    },
    reports: (state = [], action) => {
        switch (action.type) {
            case 'ALL_REPORT':
                return action.payload;
            case 'VIEW_REPORT' :
                const report = action.payload;
                const indexvr = state.findIndex((reportItem) => reportItem.id === report.id);
                if (indexvr !== -1) {
                    state[index] = tenant;
                }
                return state;
            case 'UPDATE_REPORT':
                const indexur = state.findIndex((tenant) => tenant.id === action.payload.id);
                state[indexur] = action.payload;
                return state;
            default:
                return state;
        }
    },
};

const store = createStore(reducers);
export default store;
