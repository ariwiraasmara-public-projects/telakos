<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class astela_1002_tenant extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'astela_1002_tenant';
    protected $primaryKey = 'id1002';
    public $incrementing = false;
    protected $fillable = ['id1002',
                            'id1003',
                            'nama',
                            'tlp',
                            'alamat',
                            'tgl_bayar',
                            'jumlah',
                            'tipe',
                            'sk',
                            'is_active',
                            'keterangan'];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';

}
