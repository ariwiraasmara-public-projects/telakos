<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class astela_1003_monthendreport extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'astela_1003_monthendreport';
    protected $primaryKey = 'id1003';
    public $incrementing = false;
    protected $fillable = ['id1003',
                            'date',
                            'keterangan',
                            'amount',];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
