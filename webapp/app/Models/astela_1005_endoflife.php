<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class astela_1005_endoflife extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = 'astela_1005_endoflife';
    protected $primaryKey = 'id1002';
    public $incrementing = false;
    protected $fillable = ['id1002',
                            'id1004',
                            'tgl',];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
