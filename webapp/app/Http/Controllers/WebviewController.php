<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Services\astela_1002_tenantService;
use App\Services\astela_1003_monthendreportService;
use App\Services\astela_1004_roomService;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Libraries\myfunction;
use Cookie;
use Redirect;

class WebviewController extends Controller {
    //

    public function __construct(protected UserService $user,
                                protected astela_1002_tenantService $tenant,
                                protected astela_1003_monthendreportService $report,
                                protected astela_1004_roomService $room,
                                protected myfunction $fun,
                                protected $pislogin = null) {

    }

    protected function islogin() {
        if(isset($_COOKIE['islogin'])) {
            $this->pislogin = 1;
            return Redirect::to('/teh/dashboard');
        }
        $this->pislogin = 0;
    }

    protected function isnotlogin() {
        if(!isset($_COOKIE['islogin'])) {
            $this->pislogin = 0;
            return Redirect::to('/teh/login');
        }
        $this->pislogin = 1;
    }

    public function login() {
        $this->islogin();
        return Inertia::render('Login', [
            'title' => 'Login',
            'islogin' => $this->pislogin
        ]);
    }

    public function register() {
        $this->islogin();
        return Inertia::render('Register', [
            'title' => 'Daftar',
            'islogin' => $this->pislogin
        ]);
    }

    public function forgotpassword() {
        // $this->islogin();
        return Inertia::render('Forgotpassword', [
            'title' => 'Lupa Password',
            'islogin' => $this->pislogin
        ]);
    }

    public function dashboard() {
        $this->isnotlogin();
        return Inertia::render('Dashboard', [
            'title' => 'Dashboard',
            'islogin' => $this->pislogin
        ]);
    }

    public function allTenant() {
        $islogin = $this->fun->getCookie('islogin');
        return Inertia::render('Tenang/All', [
            'title' => 'Semua Penyewa',
            'islogin' => $islogin
        ]);
    }

    public function createTenant() {
        $this->isnotlogin();
        return Inertia::render('Tenant/CreateEdit', [
            'title' => 'Penyewa Baru',
            'islogin' => $this->pislogin,
            'listroom' => $this->room->getAllActive()
        ]);
    }

    public function editTenant() {
        $this->isnotlogin();
        return Inertia::render('Tenant/CreateEdit', [
            'title' => 'Penyewa Baru',
            'islogin' => $this->pislogin,
            'listroom' => $this->room->getAllActive(),
            'isedit' => 1
        ]);
    }

    public function getTenant($id) {
        $islogin = $this->fun->getCookie('islogin');
        return Inertia::render('Tenant/View', [
            'title' => 'Lihat Kwitansi',
            'islogin' => $this->pislogin,
            'id' => $id,
            'mid' => $islogin
        ]);
    }

    public function allReport() {
        $this->isnotlogin();
        return Inertia::render('Report/All', [
            'title' => 'Semua Laporan',
            'islogin' => $this->pislogin
        ]);
    }

    public function createReport() {
        $this->isnotlogin();
        return Inertia::render('Report/Create', [
            'title' => 'Laporan Baru',
            'islogin' => $this->pislogin
        ]);
    }

    public function viewReport() {
        return Inertia::render('Report/View', [
            'title' => 'Lihat Laporan',
            'islogin' => $this->pislogin
        ]);
    }

    public function profile() {
        $this->isnotlogin();
        return Inertia::render('Profile/View', [
            'title' => 'Profil',
            'islogin' => $this->pislogin
        ]);
    }

}
