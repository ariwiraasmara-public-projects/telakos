<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\astela_1002_tenantService;
use App\Services\astela_1003_monthendreportService;

use File;
use App\Libraries\myfunction;
use App\Libraries\jsr;

class APIController extends Controller {

    public function __construct(protected UserService $user,
                                protected astela_1002_tenantService $tenant,
                                protected astela_1003_monthendreportService $report,
                                protected myfunction $fun,
                                protected jsr $jsr) {

    }

    public function login(Request $request) {
        $validated = $request->validate([
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);

        if(!$validated) {
            return $this->jsr->r(['msg' => 'Invalid!', 'error' => 1], 'bad request');
        }

        $data = $this->user->login($request->email, $request->password);
        if($data->get('success')) {
            $this->cfun->setCookie([
                'islogin'   => 1,
                "email"     => $data['success'][0]['email'],
            ], true, 1, 24, 60, 60);

            return $this->jsr->r(['msg' => 'You\'re Loged In', 'success'=> 1, 'data'=>$data['success'][0]], 'success');
        }

        return match($data->get('error')){
            1 => $this->jsr->r(['msg' => 'Wrong Email', 'error'=> 1], 'bad request'),
            2 => $this->jsr->r(['msg' => 'Wrong Password', 'error'=> 2], 'bad request'),
            default =>$this->jsr->r(['msg' => 'Something\'s Wrong!', 'error'=> -1], 'bad request')
        };
    }

    public function register(Request $request) {
        $validated = $request->validate([
            'name'     => 'required|string',
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);

        if(!$validated) {
            return $this->jsr->r(['msg' => 'Invalid!', 'error' => 1], 'bad request');
        }

        $check = $this->user->get(['email' => $request->email]);
        if($check) {
            return $this->jsr->r(['msg' => 'This User Alread Registered!', 'error'=> 1], 'bad request');
        }

        $res = $this->user->store([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => $this->fun->encrypt($this->request->password)
        ]);

        if($res > 0) return $this->jsr->r(['msg' => 'Register Success!', 'success' => 1], 'success');
        return $this->jsr->r(['msg' => 'Register Fail!', 'error' => 1], 'bad request');
    }

    public function dashboard() {
        $email = $this->fun->getCookie('email');

        $recent_tenant = $this->tenant->getAll('date', 'desc', 6);
        if($recent_tenant == 0) $recent_tenant = '__none__';

        $recent_report = $this->report->getAllResume('date', 'desc', 6);
        if($recent_report == 0) $recent_tenant = '__none__';

        return $this->jsr->r([
            'msg' => 'Dashboard',
            'success' => 1,
            'data' => [
                'recent_tenant' => $recent_tenant,
                'recent_report' => $recent_report
            ]
        ], 'success');
    }

    public function allTenant($orderby = 'date', $asc = 'desc') {
        return $this->jsr-r([
            'msg' => 'All Tenant',
            'success' => 1,
            'data' => $this->tenant->getAll($orderby, $asc)
        ], 'success');
    }

    public function getTenant($id) {
        $data = $this->tenant->get(['id1002' => $id]);
        return $this->jsr->r([
            'msg' => 'One Tenant You\'re Looking For!',
            'success' => 1,
            'directory' => $this->user->readDir($data[0]['nama']),
            'data' => $data
        ], 'success');
    }

    public function createTenant() {
        $validated = $request->validate([
            'nama'   => 'required|string',
            'tlp'    => 'required|string',
            'amount' => 'required|integer',
            'type'   => 'required|integer',
            'sk'     => 'required'
        ]);

        if(!$validated) {
            return $this->jsr->r(['msg' => 'Invalid!', 'error' => 1], 'bad request');
        }

        $res = $this->tenant->store($data);
        if($res > 0) {
            $this->user->createDir($request->nama);
            $file = $request->file('sk');
            $file->move($this->user->readDir($request->nama), $file->getClientOriginalName());
            return $this->jsr->r(['msg' => 'Success Store New Tenant!', 'success' => 1]);
        }
        return $this->jsr->r(['msg' => 'Fail to Store New Tenant!', 'error' => 1]);
    }

    public function updateTenant(Request $request, $id) {
        if(is_null($id) || empty($id)) return $this->jsr->r(['msg' => 'Invalid!', 'error' => 1], 'bad request');

        $validated = $request->validate([
            'jumlah' => 'required|integer',
            'tipe'   => 'required|integer',
        ]);

        $data = $this->tenant->get($id);
        $res = $this->tenant->update([
            'tgl_bayar'  => $request->tgl_bayar,
            'jumlah'     => $request->jumlah,
            'tipe'       => $request->tipe,
            'keterangan' => $request->keterangan
        ], $id);

        if($res) {
            // $file = $request->file('sk');
            // if($data[0]['nama'] != $request->nama) {
            //     $this->user->deleteDir($data[0]['nama']);
            //     $this->user->createDir($request->nama);
            // }

            // if($data[0]['sk'] != $file->getClientOriginalName()) {
            //     $this->user->deleteFile($data[0]['sk']);
            //     $file->move($this->user->readDir($request->nama), $file->getClientOriginalName());
            // }

            return $this->jsr->r(['msg' => 'Success Update Tenant!', 'success' => 1]);
        }
        return $this->jsr->r(['msg' => 'Fail to Update Tenant!', 'error' => 1]);
    }

    public function updateTenantActive(Request $request, $id) {
        if(is_null($id) || empty($id)) return $this->jsr->r(['msg' => 'Invalid!', 'error' => 1], 'bad request');

        $validated = $request->validate([
            'nama'   => 'required|string',
            'tlp'    => 'required|string',
        ]);

        if(!$validated) {
            return $this->jsr->r(['msg' => 'Invalid!', 'error' => 2], 'bad request');
        }

        $res = $this->room->update([
            'is_active' => $request->is_active
        ], $id);
    }

    public function allReport($asc = 'desc') {
        return $this->jsr-r([
            'msg' => 'All Report',
            'success' => 1,
            'data' => $this->report->getAll('date', $asc)
        ], 'success');
    }

    public function createReport(Request $request) {
        // di fungsi akan menerima value berupa array dari frontendnya
        /**
         * $request->date[]
         * $request->keterangan[]
         * $request->amount[]
         */
        // $res = $this->report->store();
    }

    public function viewReport($date) {
        return $this->jsr->r([
            'msg' => 'One Tenant You\'re Looking For!',
            'success' => 1,
            'data' => $this->report->viewReport($date)
        ], 'success');
    }

    public function allRoom() {

    }

    public function getRoom() {

    }

    public function updateRoom() {

    }

}
