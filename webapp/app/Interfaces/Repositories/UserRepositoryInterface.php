<?php
namespace App\Interfaces\Repositories;

use App\Models\User;
interface UserRepositoryInterface {

    public function __construct(User $model);
    public function get(array $data);
    public function store(array $data);
    public function update(array $data, int $id);
    public function delete(int $id);
    public function createDir(String $name = '');
    public function readDir(String $name = ''): String;
    public function deleteDir(String $name = '');
    public function deleteFile(String $name, String $file);

}
?>
