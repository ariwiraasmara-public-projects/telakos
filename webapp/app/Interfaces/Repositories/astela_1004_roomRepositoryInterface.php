<?php
namespace App\Interfaces\Repositories;

use App\Models\astela_1004_room;
interface astela_1004_roomRepositoryInterface {

    public function __construct(astela_1004_room $model);
    public function getAll();
    public function getAllActive();
    public function get(array $data);
    public function store(array $data);
    public function update(array $data, String $id);
    public function delete(String $id);

}
?>
