<?php
namespace App\Interfaces\Repositories;

use App\Models\astela_1003_monthendreport;
interface astela_1003_monthendreportRepositoryInterface {

    public function __construct(astela_1003_monthendreport $model);
    public function getAllResume(String $order = 'date', String $asc = 'desc', int $limit = 0);
    public function get(String $date);
    public function sumTotal(String $month);
    public function store(array $data);
    public function update(array $data, String $id);
    public function delete(String $id);

}
?>
