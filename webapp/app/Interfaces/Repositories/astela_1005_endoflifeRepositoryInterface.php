<?php
namespace App\Interfaces\Repositories;

use App\Models\astela_1005_endoflife;
interface astela_1005_endoflifeRepositoryInterface {

    public function __construct(astela_1005_endoflife $model);
    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0);
    public function get(String $data);
    public function store(array $data);
    public function update(array $data, String $id);
    public function delete(String $id);

}
?>
