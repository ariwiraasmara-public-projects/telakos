<?php
namespace App\Interfaces\Repositories;

use App\Models\astela_1002_tenant;
interface astela_1002_tenantRepositoryInterface {

    public function __construct(astela_1002_tenant $model);
    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0);
    public function get(String $id);
    public function getEndTenantReport(String $date);
    public function getEndTotalReport(String $date);
    public function sumTotal(String $date);
    public function store(array $data);
    public function update(array $data, String $id);
    public function delete(String $id);

}
?>
