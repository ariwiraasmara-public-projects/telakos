<?php
namespace App\Interfaces\Services;

use App\Repositories\astela_1004_roomRepository;
interface astela_1004_roomServiceInterface {

    public function __construct(astela_1004_roomRepository $repo);
    public function getAll();
    public function getAllActive();
    public function get(String $id);
    public function store(array $data);
    public function update(array $data, String $id);
    public function delete(String $id);
    public function viewReport(String $month);

}
?>
