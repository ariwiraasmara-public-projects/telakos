<?php
namespace App\Interfaces\Services;

use App\Repositories\astela_1002_tenantRepository;
use App\Repositories\astela_1004_roomRepository;
use App\Repositories\astela_1005_endoflifeRepository;
use File;
interface astela_1002_tenantServiceInterface {

    public function __construct(astela_1002_tenantRepository $repo,
                                astela_1004_roomRepository $room,
                                astela_1005_endoflifeRepository $eol,
                                String $path = '');
    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0);
    public function get(int $id);
    public function getEndTenantReport(String $date);
    public function getEndTotalReport(String $date);
    public function sumTotal(String $date);
    public function store(array $data);
    public function update(array $data, String $id);
    public function delete(String $id);

}
?>
