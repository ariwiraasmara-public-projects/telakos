<?php
namespace App\Interfaces\Services;

use App\Repositories\astela_1005_endoflifeRepository;
interface astela_1005_endoflifeServiceInterface {

    public function __construct(astela_1005_endoflifeRepository $repo);
    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0);
    public function get(String $id);
    public function store(array $data);
    public function update(array $data, String $id);
    public function delete(String $id);

}
?>
