<?php
namespace App\Interfaces\Services;

use App\Repositories\astela_1002_tenantRepository;
use App\Repositories\astela_1003_monthendreportRepository;
interface astela_1003_monthendreportServiceInterface {

    public function __construct(astela_1002_tenantRepository $tenant,
                                astela_1003_monthendreportRepository $repo);
    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0);
    public function get(String $id);
    public function sumTotal(String $month);
    public function store(array $data);
    public function update(array $data, String $id);
    public function delete(String $id);

}
?>
