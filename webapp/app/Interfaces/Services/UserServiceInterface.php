<?php
namespace App\Interfaces\Services;

use App\Repositories\UserRepository;
use App\Libraries\myfunction;
interface UserServiceInterface {

    public function __construct(UserRepository $repo, myfunction $fun);
    public function get(int $id);
    public function login(String $email, String $password);
    public function store(array $data);
    public function update(array $data, int $id);
    public function delete(int $id);
    public function createDir(String $name = '');
    public function readDir(String $name = ''): String;
    public function deleteDir(String $name = '');
    public function deleteFile(String $name, String $file);

}
?>
