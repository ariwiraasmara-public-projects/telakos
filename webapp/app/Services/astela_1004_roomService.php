<?php
namespace App\Services;

use App\Interfaces\Services\astela_1004_roomServiceInterface;
use App\Repositories\astela_1004_roomRepository;
class astela_1004_roomService implements astela_1004_roomServiceInterface {

    public function __construct(protected astela_1004_roomRepository $repo) {

    }

    public function getAll() {
        return $this->repo->getAll();
    }

    public function getAllActive() {
        return $this->repo->getAllActive();
    }

    public function get(String $id) {
        return $this->repo->get($id);
    }

    public function store(array $data) {
        return $this->repo->store($data);
    }

    public function update(array $data, String $id) {
        return $this->repo->update($data, $id);
    }

    public function delete(String $id) {
        return $this->repo->delete($id);
    }

    public function viewReport(String $month) {
        $tenant_amount_total = $this->tenant->sumTotal($month);
        $report_amount_total = $this->repo->sumTotal($month);
        $final_total = $tenant_amount_total['total'] - $report_amount_total['total'];
        return collect([
            'tenants' => $this->tenant->getAll(['date' => `MONTH($month)`]),
            'total' => $tenant_amount_total,
            'report detail' => $this->repo->get(['date' => `MONTH($month)`]),
            'final total' => $final_total
        ]);
    }

}
?>
