<?php
namespace App\Services;

use App\Interfaces\Services\astela_1003_monthendreportServiceInterface;
use App\Repositories\astela_1002_tenantRepository;
use App\Repositories\astela_1003_monthendreportRepository;
class astela_1003_monthendreportService implements astela_1003_monthendreportServiceInterface {

    public function __construct(protected astela_1002_tenantRepository $tenant,
                                protected astela_1003_monthendreportRepository $repo) {

    }

    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0) {
        return $this->repo->getAll($order, $asc, $limit);
    }

    public function get(String $id) {
        return $this->repo->get($id);
    }

    public function sumTotal(String $month) {
        return $this->repo->sumTotal($month);
    }

    public function store(array $data) {
        return $this->repo->store($data);
    }

    public function update(array $data, String $id) {
        return $this->repo->update($data, $id);
    }

    public function delete(String $id) {
        return $this->repo->delete($id);
    }

    public function viewReport(String $date) {
        $tenant_amount_total = $this->tenant->sumTotal($date);
        $report_amount_total = $this->repo->sumTotal($date);
        $final_total = $tenant_amount_total['total'] - $report_amount_total['total'];
        return collect([
            'tenant' => [
                'data' => $this->tenant->getEndTenantReport($date),
                'each_total' => $this->tenant->getEndTotalReport($date),
                'total_in_a_month' => $tenant_amount_total
            ],
            'detail' => [
                'data' => $this->repo->get($date),
                'total' => $report_amount_total
            ],
            'final total' => $final_total
        ]);
    }

}
?>
