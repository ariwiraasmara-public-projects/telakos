<?php
namespace App\Services;

use App\Interfaces\Services\UserServiceInterface;
use App\Repositories\UserRepository;
use App\Libraries\myfunction;
class UserService implements UserServiceInterface {

    public function __construct(protected UserRepository $repo, protected myfunction $fun) {

    }

    public function get(int $id) {
        return $this->repo->get($id);
    }

    public function login(String $email, String $password) {
        $check = $this->repo->get(['email'=>$email]);
        if($check) {
            if( $password == $this->fun->decrypt($check[0]['password']) ) {
                return $this->jsr->r(['success' => $check]);
            }
            return collect(['error'=>2]); //'Wrong Password';
        }
        return collect(['error'=>1]); //'Wrong Username / Email';
    }

    public function store(array $data) {
        return $this->repo->store($data);
    }

    public function update(array $data, int $id) {
        return $this->repo->update($data, $id);
    }

    public function delete(int $id) {
        return $this->repo->delete($id);
    }

    public function createDir(String $name = '') {
        return $this->repo->createDir($name);
    }

    public function readDir(String $name = ''): String {
        return $this->repo->readDir($name);
    }

    public function deleteDir(String $name = '') {
        return $this->repo->deleteDir($name);
    }

    public function deleteFile(String $name, String $file) {
        return $this->repo->deleteFile($name, $file);
    }

}
?>
