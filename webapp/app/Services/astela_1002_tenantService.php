<?php
namespace App\Services;

use App\Interfaces\Services\astela_1002_tenantServiceInterface;
use App\Repositories\astela_1002_tenantRepository;
use App\Repositories\astela_1004_roomRepository;
use App\Repositories\astela_1005_endoflifeRepository;
use File;
class astela_1002_tenantService implements astela_1002_tenantServiceInterface {

    public function __construct(protected astela_1002_tenantRepository $repo,
                                protected astela_1004_roomRepository $room,
                                protected astela_1005_endoflifeRepository $eol,
                                protected String $path = '') {
        $this->path = public_path('sk/');
    }

    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0) {
        return $this->repo->getAll($order, $asc, $limit);
    }

    public function get(int $id) {
        return $this->repo->get($id);
    }

    public function getEndTenantReport(String $date) {
        return $this->repo->getEndTenantReport($date);
    }

    public function getEndTotalReport(String $date) {
        return $this->repo->getEndTotalReport($date);
    }

    public function sumTotal(String $date) {
        return $this->repo->sumTotal($date);
    }

    public function store(array $data) {
        $id = 'TELAKOS'.date('Ymdhis');
        $final_date = date("Y-m-d", strtotime($data['tgl_bayar']));
        if(is_null($data['keterangan']) ||
            empty($data['keterangan']) ||
            $data['keterangan'] == '') {
                $data['keterangan'] = 'Pembayaran pertama pada '.$final_date.' dengan jumlah Rp. '.$data['amount'];
        }
        return $this->repo->store([
            'id1002'     => $id,
            'id1004'     => $data['room'],
            'nama'       => $data['nama'],
            'tlp'        => $data['tlp'],
            'tgl_bayar'  => $final_date,
            'jumlah'     => $data['amount'],
            'tipe'       => $data['tipe'],
            'sk'         => $this->path.$data['nama'].$data['sk'],
            'is_active'  => 1,
            'keterangan' => $data['keterangan'],
        ]) && $this->room->update([
            'is_active'  => 1,
            'keterangan' => $data['keterangan'],
        ], $data['room']) && $this->eol->store([
            'id1002' => $id,
            'id1004' => $data['room'],
            'tgl'    => date_add($final_date, date_interval_create_from_date_string("6 month"))
        ]);
    }

    public function update(array $data, String $id) {
        $final_date = date("Y-m-d", strtotime($data['tgl_bayar']));
        if(is_null($data['keterangan']) ||
            empty($data['keterangan']) ||
            $data['keterangan'] == '') {
                $data['keterangan'] = 'Pembayaran pertama pada '.$final_date.' dengan jumlah Rp. '.$data['amount'];
        }

        return $this->repo->update($data, $id) && $this->eol->update();
    }

    public function delete(String $id) {
        return $this->repo->delete($id);
    }

}
?>
