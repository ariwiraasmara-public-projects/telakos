<?php
namespace App\Services;

use App\Interfaces\Services\astela_1005_endoflifeServiceInterface;
use App\Repositories\astela_1005_endoflifeRepository;
class astela_1005_endoflifeService implements astela_1005_endoflifeServiceInterface {

    public function __construct(protected astela_1005_endoflifeRepository $repo) {

    }

    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0) {
        return $this->repo->getAll($order, $asc, $limit);
    }

    public function get(String $id) {
        return $this->repo->get($id);
    }

    public function store(array $data) {
        return $this->repo->store($data);
    }

    public function update(array $data, int $id) {
        return $this->repo->update($data, $id);
    }

    public function delete(int $id) {
        return $this->repo->delete($id);
    }

}
?>
