<?php
namespace App\Repositories;

use App\Interfaces\Repositories\astela_1005_endoflifeRepositoryInterface;
use App\Models\astela_1005_endoflife;
class astela_1005_endoflifeRepository implements astela_1005_endoflifeRepositoryInterface {

    public function __construct(protected astela_1005_endoflife $model) {

    }

    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0) {
        if($limit > 0) return $this->model->orderBy($order, $asc)->limit($limit)->get();
        return $this->model->orderBy($order, $asc)->get();
    }

    public function get(String $id) {
        return $this->model->where(['id1002'=>$id])->get();
    }

    public function store(array $data) {
        $res = $this->model->create($data);
        if($res > 0) return 1;
        return 0;
    }

    public function update(array $data, String $id) {
        $res = $this->model->where(['id1002' => $id])->update($data);
        if($res > 0) return 1;
        return 0;
    }

    public function delete(String $id) {
        $res = $this->model->where('id1002', '=', $id)->delete();
        if($res > 0) return 1;
        return 0;
    }

}
?>
