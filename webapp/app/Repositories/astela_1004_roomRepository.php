<?php
namespace App\Repositories;

use App\Interfaces\Repositories\astela_1004_roomRepositoryInterface;
use App\Models\astela_1004_room;
class astela_1004_roomRepository implements astela_1004_roomRepositoryInterface {

    public function __construct(protected astela_1004_room $model) {

    }

    public function getAll() {
        $data = $this->model->orderBy('room', 'asc');
        if($data->first()) return $data->get();
        return 0;
    }

    public function getAllActive() {
        $data =  $this->model->where(['is_active' => 1])->orderBy('room', 'asc');
        if($data->first()) return $data->get();
        return 0;
    }

    public function get(array $data) {
        return $this->model->where($data)->get();
    }

    public function store(array $data) {
        $res = $this->model->create($data);
        if($res > 0) return 1;
        return 0;
    }

    public function update(array $data, String $id) {
        $res = $this->model->where(['id1003' => $id])->update($data);
        if($res > 0) return 1;
        return 0;
    }

    public function delete(String $id) {
        $res = $this->model->where('id1003', '=', $id)->delete();
        if($res > 0) return 1;
        return 0;
    }

}
?>
