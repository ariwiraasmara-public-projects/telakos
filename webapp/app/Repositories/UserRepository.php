<?php
namespace App\Repositories;

use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Models\User;
class UserRepository implements UserRepositoryInterface {

    public function __construct(User $model) {
        $this->path = public_path('userfiles/');
    }

    public function get(array $data) {
        return $this->model->where($data)->get();
    }

    public function store(array $data) {
        $res = $this->model->create($data);
        if($res > 0) return 1;
        return 0;
    }

    public function update(array $data, int $id) {
        $res = $this->model->where(['id' => $id])->update($data);
        if($res > 0) return 1;
        return 0;
    }

    public function delete(int $id) {
        $res = $this->model->where('id', '=', $id)->delete();
        if($res > 0) return 1;
        return 0;
    }

    public function createDir(String $name = '') {
        $final = $this->path.$name.'/';
        if(!File::isDirectory($final)) return File::makeDirectory($final, 0777, true, true);
    }

    public function readDir(String $name = ''): String {
        return $this->path.$name.'/';
    }

    public function deleteDir(String $name = '') {
        $final = $this->path.$name.'/';
        if(File::exists($final)) return File::deleteDirectory($final, 0777, true, true);
    }

    public function deleteFile(String $name, String $file) {
        return File::delete($this->path.$name.'/'.$file);
    }

}
?>
