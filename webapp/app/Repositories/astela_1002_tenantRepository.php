<?php
namespace App\Repositories;

use App\Interfaces\Repositories\astela_1002_tenantRepositoryInterface;
use App\Models\astela_1002_tenant;
class astela_1002_tenantRepository implements astela_1002_tenantRepositoryInterface {

    public function __construct(protected astela_1002_tenant $model) {

    }

    protected function dataJoin(String $order = 'date', String $asc = 'desc') {
        return $this->model->select('astela_1002_tenant.id1002 as id1002', 'astela_1004_room.id1004 as id1004',
                                    'astela_1004_room.room as kamar', 'astela_1002_tenant.nama as nama',
                                    'astela_1002_tenant.tlp as tlp', 'astela_1002_tenant.alamat as alamat',
                                    'astela_1002_tenant.tgl_bayar as tgl_bayar', 'astela_1005_endoflife.tgl as tgl_batas_akhir',
                                    'astela_1002_tenant.jumlah as jumlah', 'astela_1002_tenant.tipe as tipe',
                                    'astela_1002_tenant.is_active as penyewa_aktif', 'astela_1004_room.is_active as kamar_aktif', 'astela_1002_tenant.sk as sk',
                                    'astela_1002_tenant.keterangan as keterangan_penyewa', 'astela_1004_room.keterangan as keterangan_kamar')
                            ->orderBy($order, $asc)
                            ->join('astela_1004_room', 'astela_1004_room.id1004', '=', 'astela_1002_tenant.id1004')
                            ->join('astela_1005_endoflife', 'astela_1005_endoflife.id1002', '=', 'astela_1002_tenant.id1002');
    }

    public function getAll(String $order = 'date', String $asc = 'desc', int $limit = 0) {
        if($this->model->first()) {
            if($limit > 0) return $this->dataJoin()->limit($limit)->get();
            return $this->dataJoin()->get();
        }
        return 0;
    }

    public function get(String $id) {
        if($this->dataJoin()->where(['astela_1002_tenant.id1002'=>$id])->first()) {
            return $this->dataJoin()->where(['astela_1002_tenant.id1002'=>$id])->get();
        }
        return 0;
    }

    public function getEndTenantReport(String $date) {
        $data = $this->model->select()->whereBetween('date', `DATE_FORMAT($date, '%Y-%m-01')`, `LAST_DAY($date)`);
        if($data->first()) return $data->get();
        return 0;
    }

    public function getEndTotalReport(String $date) {
        $data = $this->model->select(`SUM(SELECT jumlah where tipe=1 and date between DATE_FORMAT($date, '%Y-%m-01') and LAST_DAY($date)) as total_1_bulan`,
                                     `SUM(SELECT jumlah where tipe=6 and date between DATE_FORMAT($date, '%Y-%m-01') and LAST_DAY($date)) as total_6_bulan`,
                                     `SUM(SELECT jumlah where tipe=12 and date between DATE_FORMAT($date, '%Y-%m-01') and LAST_DAY($date)) as total_12_bulan`
                                    )
                            ->whereBetween('date', `DATE_FORMAT($date, '%Y-%m-01')`, `LAST_DAY($date)`);
        if($data->first()) $data->get();
        return 0;
    }

    public function sumTotal(String $date) {
        $data = $this->model->select('sum(jumlah) as total')->whereBetween('date', `DATE_FORMAT($date, '%Y-%m-01')`, `LAST_DAY($date)`);
        if($data->first()) return $data->get();
        return 0;
    }

    public function store(array $data) {
        $res = $this->model->create($data);
        if($res > 0) return 1;
        return 0;
    }

    public function update(array $data, String $id) {
        $res = $this->model->where(['id1002' => $id])->update($data);
        if($res > 0) return 1;
        return 0;
    }

    public function delete(String $id) {
        $res = $this->model->where('id1002', '=', $id)->delete();
        if($res > 0) return 1;
        return 0;
    }

}
?>
