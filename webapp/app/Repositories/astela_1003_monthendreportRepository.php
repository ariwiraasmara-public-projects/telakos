<?php
namespace App\Repositories;

use App\Interfaces\Repositories\astela_1003_monthendreportRepositoryInterface;
use App\Models\astela_1003_monthendreport;
class astela_1003_monthendreportRepository implements astela_1003_monthendreportRepositoryInterface {

    public function __construct(protected astela_1003_monthendreport $model) {

    }

    public function getAllResume(String $order = 'date', String $asc = 'desc', int $limit = 0) {
        if($this->model->first()) {
            $data = $this->model->select("DISTINCT DATE_FORMAT(date, '%Y %M') as bulan, LAST_DAY(date) end_day_of_month")->orderBy($order, $asc);
            if($limit > 0) return $data->limit($limit)->get();
            return $data->get();
        }
        return 0;
    }

    public function get(String $date) {
        $data = $this->model->whereBetween('date', `DATE_FORMAT($date, '%Y-%m-01')`, `LAST_DAY($date)`);
        if($data->first()) {
            return $data->get();
        }
        return 0;
    }

    public function sumTotal(String $date) {
        return $this->model->select('sum(jumlah) as total')
                ->whereBetween('date', `DATE_FORMAT($date, '%Y-%m-01')`, `LAST_DAY($date)`)
                ->get();
    }

    public function store(array $data) {
        $res = $this->model->create($data);
        if($res > 0) return 1;
        return 0;
    }

    public function update(array $data, String $id) {
        $res = $this->model->where(['id1003' => $id])->update($data);
        if($res > 0) return 1;
        return 0;
    }

    public function delete(String $id) {
        $res = $this->model->where('id1003', '=', $id)->delete();
        if($res > 0) return 1;
        return 0;
    }

}
?>
