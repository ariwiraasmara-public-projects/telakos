# Thanks To
- Gitlab (https://gitlab.com), as control version
- Laravel (https://laravel.com/), as backend
- ReactJS (https://react.dev/), as frontend
- Material UI (https://mui.com/), as UI framework
- MySQL (https://www.mysql.com/), as database

# @ Copyright ***Syahri Ramadhan Wiraasmara (ARI)***
