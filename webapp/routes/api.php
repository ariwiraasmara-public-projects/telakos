<?php
use App\Http\Controllers\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/login', 'App\Http\Controllers\APIController@login');
Route::post('/register', 'App\Http\Controllers\APIController@register');
Route::post('/dashboard', 'App\Http\Controllers\APIController@dashboard');
Route::post('/report/{month}', 'App\Http\Controllers\APIController@viewReport');
Route::get('/tenant/{orderby}/{asc}', 'App\Http\Controllers\APIController@allTenant');
Route::get('/tenant/{id}', 'App\Http\Controllers\APIController@getTenant');
Route::post('/tenant/store', 'App\Http\Controllers\APIController@storeTenant');
Route::put('/tenant/update/{id}', 'App\Http\Controllers\APIController@updateTenant');
Route::put('/tenant/update/active/{id}', 'App\Http\Controllers\APIController@updateTenantActive');
Route::delete('/tenant/delete/{id}', 'App\Http\Controllers\APIController@deleteTenant');
Route::get('/report/all/{asc}', 'App\Http\Controllers\APIController@allReport');
Route::get('/report/{date}', 'App\Http\Controllers\APIController@viewReport');
Route::post('/report/store', 'App\Http\Controllers\APIController@storeReport');
Route::put('/report/update/{id}', 'App\Http\Controllers\APIController@updateReport');
Route::delete('/report/delete/{id}', 'App\Http\Controllers\APIController@deleteReport');
Route::get('/room', 'App\Http\Controllers\APIController@allRoom');
Route::get('/room/{id}', 'App\Http\Controllers\APIController@getRoom');
Route::put('/room/update/{id}', 'App\Http\Controllers\APIController@updateRoom');
