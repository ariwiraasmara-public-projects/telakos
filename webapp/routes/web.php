<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\WebviewController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/teh/login', 'App\Http\Controllers\WebviewController@login');
Route::get('/teh/register', 'App\Http\Controllers\WebviewController@register');
Route::get('/teh/lupapassword', 'App\Http\Controllers\WebviewController@forgotpassword');
Route::get('/teh/dashboard', 'App\Http\Controllers\WebviewController@dashboard');
Route::get('/teh/profil', 'App\Http\Controllers\WebviewController@profile');
Route::get('/public/penyewa', 'App\Http\Controllers\WebviewController@allTenant');
Route::get('/teh/penyewa', 'App\Http\Controllers\WebviewController@allTenant');
Route::get('/teh/penyewa/{id}', 'App\Http\Controllers\WebviewController@getTenant');
Route::get('/teh/penyewa/baru', 'App\Http\Controllers\WebviewController@createTenant');
Route::get('/teh/penyewa/edit/{id}', 'App\Http\Controllers\WebviewController@editTenant');
Route::get('/teh/laporan', 'App\Http\Controllers\WebviewController@allReport');
Route::get('/teh/laporan/{date}', 'App\Http\Controllers\WebviewController@viewReport');
Route::get('/teh/laporan/baru', 'App\Http\Controllers\WebviewController@createReport');

require __DIR__.'/auth.php';
