# Thanks To
- Gitlab (https://gitlab.com), as control version
- Kotlin (https://kotlinlang.org/), as programming language used
- Firebase (https://firebase.google.com/), as database

# @ Copyright ***Syahri Ramadhan Wiraasmara (ARI)***
